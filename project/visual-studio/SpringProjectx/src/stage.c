/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);
static void updateTime();

int countsec = 0; // Seconds since start
int rawtime = 0; // Raw rime
int limit = 5; // Game limit time
int timeleft = 0; // Game time left

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();
}

static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 154, 207, 247, 255);
	SDL_RenderFillRect(app.renderer, NULL);
	SDL_TimerID timer = SDL_AddTimer(1000, updateTime, NULL);

	drawMap();

	drawEntities();

	drawHud();
}
// NOT YOURS!!
static void updateTime() {
	rawtime = SDL_GetTicks();
	countsec = rawtime / 1000 % 60;
}

static void drawHud(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH - 5, 3, 255, 255, 255, TEXT_RIGHT, "PIZZA : %d/%d", stage.pizzaFound, stage.pizzaTotal);

	updateTime();
	timeleft = limit - countsec;
	drawText(SCREEN_WIDTH / 20 - 45, 5, 255, 255, 255, TEXT_LEFT, "SECONDS : %d ", timeleft);

	if (timeleft == 0)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0, 43, 255, TEXT_CENTER, "YOU'VE RAN OUT OF TIME. GAME OVER! PRESS X TO EXIT!");
	}
		if (stage.pizzaFound == stage.pizzaTotal)
		{
			playSound(SND_PIZZA_DONE, CH_PIZZA);
			if (stage.pizzaFound == stage.pizzaTotal)
			{
				drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0, 43, 255, TEXT_CENTER, "YAY! YOU FOUND ALL THE PIZZA! PRESS X TO EXIT!");
			}

		}
}