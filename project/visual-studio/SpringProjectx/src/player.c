/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static SDL_Texture* lozz[4];

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));

	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 1;
	// Jump left
	lozz[0] = loadTexture("gfx/lozz1.png");
	// Jump right
	lozz[1] = loadTexture("gfx/lozz2.png");
	// Run left
	lozz[2] = loadTexture("gfx/lozz4.png");
	// Run right
	lozz[3] = loadTexture("gfx/lozz3.png");

	player->texture = lozz[1];

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;
		player->facing = 1;
		player->texture = lozz[1];
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;
		player->facing = 0;
		player->texture = lozz[0];
	}

	// Jump
	if (player->isOnGround)
	{
		player->texture = player->facing ? lozz[3] : lozz[2];

		if (app.keyboard[SDL_SCANCODE_SPACE] && player->isOnGround)
		{
			player->riding = NULL;
			player->dy = -20;
			player->texture = lozz[1];
			playSound(SND_JUMP, CH_PLAYER);
		}

		if (app.keyboard[SDL_SCANCODE_D] && app.keyboard[SDL_SCANCODE_SPACE] && player->riding != NULL) {
			player->texture = lozz[2];
		}
		if (app.keyboard[SDL_SCANCODE_A] && app.keyboard[SDL_SCANCODE_SPACE] && player->riding != NULL) {
			player->texture = lozz[3];
		}
	}


	// If statement for shift left speed boost
	if (app.keyboard[SDL_SCANCODE_LSHIFT] && app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -10;
	}
	// If statment for shift right speed boost
	if (app.keyboard[SDL_SCANCODE_LSHIFT] && app.keyboard[SDL_SCANCODE_D]) 
	{
		player->dx = 10;
	}

	// Restart Game
	if (app.keyboard[SDL_SCANCODE_R])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_R] = 0;
	}
}
